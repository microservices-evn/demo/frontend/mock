const { ApolloServer } = require('@apollo/server');
const { startStandaloneServer } = require('@apollo/server/standalone');
const { find } = require('lodash');
const { v4: uuidv4 } = require('uuid');

const typeDefs = `#graphql
  type Transaction {
    id: String
    bankName: String
    amount: Float
    type: String
  }

  type User {
    id: String
    accountNumber: String
    displayName: String
    password: String
    balance: Float
  }

  type Query {
    transaction(id: String): Transaction
    transactions: [Transaction]
    user(id: String): User
  }

  type Login {
    success: Boolean
    user: User
  }

  type Mutation {
    addTransaction(type: String, bankName: String, amount: Float): Transaction
    login(accountNumber: String, password: String): Login
  }
  
`;

const transactions = []

const users = [
  {
    id: "d9ce9034-4ee0-4d50-b42f-b5ab37ecd25d",
    accountNumber: 'A12345',
    password: '12345678',
    displayName: 'Hoa Tran',
    balance: 45000000,
  }
]


const resolvers = {
  Query: {
    transactions: () => transactions,
    transaction: (_, args) => {
      const { id } = args;
      return find(transactions, { id });
    },
    user: (_, args) => {
      const { id } = args;
      return find(users, { id });
    },
  },
  Mutation: {
    login: (_, args) => {
      const { accountNumber, password } = args;
      const user = find(users, { accountNumber, password });
      return {
        success: user ? true : false,
        user
      };
    },
    addTransaction: async (_, args) => {
      const data = { id: uuidv4(), ...args}
      transactions.push(data);
      return data;
    }
  }
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
});

startStandaloneServer(server, {
  listen: { port: 4000 },
}).then(({ url }) => {
  console.log(`Server ready at ${url}`);
});
